//
// Created by SIARHEI TSISHKEVICH on 11.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "Ant.h"
#import "AntFighter.h"
#import "Types.h"
#import "AntHillView.h"

@implementation AntFighter
{

}
- (id)init
{
    self = [super init];
    if (self)
    {
        self.image = [UIImage imageNamed:@"Fighter.png"];
        self.maxAge = [[[NSUserDefaults standardUserDefaults] objectForKey:@"kMaxFighterAge"] unsignedIntegerValue];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.anthillView drawAnt:self];});
    return self;
}

@end