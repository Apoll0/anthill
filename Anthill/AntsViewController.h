//
//  AntsViewController.h
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//
@class Stats;
#import <UIKit/UIKit.h>
#import "Types.h"

@class AntHillView;

@interface AntsViewController : UIViewController

@property(nonatomic, strong) NSMutableSet *hive;
@property (strong, nonatomic) AntHillView *mainView;
@property Stats *stats;

- (void) addAnt: (AntType) type;

@end