//
//  Ant.m
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#define kTimeToPerformAction 5

#import "Ant.h"
#import <AVFoundation/AVFoundation.h>
#import "AntHillView.h"

@implementation Ant
- (id)init
{
    self = [super init];
    if (self)
    {
        _age = 0;
        _isNearDeath = NO;
    }
    return self;
}

- (void)startLiving
{
    self.location = self.anthillView.center;
    self.border = self.anthillView.bounds;

    //Init main movement thread
    self.timeToMove = [[NSUserDefaults standardUserDefaults] floatForKey:@"kMoveTime"];
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    self.moveTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, globalQueue);
    if (self.moveTimer)
    {
        dispatch_source_set_timer(self.moveTimer, dispatch_time(DISPATCH_TIME_NOW, self.timeToMove*NSEC_PER_SEC), self.timeToMove*NSEC_PER_SEC, 0);
        dispatch_source_set_event_handler(self.moveTimer,
                ^{
                    [self makeRandomMove];
                });
    }
    dispatch_resume(self.moveTimer);

    //Init action thread
    self.actionTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, globalQueue);
    if (self.actionTimer)
    {
        dispatch_source_set_timer(self.actionTimer, dispatch_time(DISPATCH_TIME_NOW, kTimeToPerformAction*NSEC_PER_SEC), kTimeToPerformAction*NSEC_PER_SEC, 0);
        dispatch_source_set_event_handler(self.actionTimer,
                ^{
                    [self performAction];
                });
    }
    dispatch_resume(self.actionTimer);
}

- (void)stopLiving
{
    if (self.moveTimer) dispatch_source_cancel(self.moveTimer);
    if (self.actionTimer) dispatch_source_cancel(self.actionTimer);

}

- (void)performAction
{
    [self playTickSound];
}

- (void)playTickSound
{
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"tick" ofType:@"wav"];
    SystemSoundID sound;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundPath],&sound);
    AudioServicesPlaySystemSound(sound);
}

- (void)makeRandomMove
{
    CGPoint newCoords = self.location;
    NSUInteger maxMoveRange = [[[NSUserDefaults standardUserDefaults] objectForKey:@"kMaxMovementRange"] unsignedIntegerValue];
    //int displX = (arc4random_uniform(maxMoveRange)+ (arc4random_uniform(4)-2)) - (maxMoveRange/2);
    //int displY = (arc4random_uniform(maxMoveRange)+ (arc4random_uniform(4)-2)) - (maxMoveRange/2);

    int displX = (arc4random_uniform(maxMoveRange+1)+1-round(maxMoveRange/2+0.5));
    int displY = (arc4random_uniform(maxMoveRange+1)+1- round(maxMoveRange/2+0.5));
    if ((newCoords.x + displX < self.border.origin.x) ||(newCoords.x + displX > (self.border.size.width-self.image.size.width))) displX = - displX;
    if ((newCoords.y + displY < self.border.origin.y) ||(newCoords.y + displY > (self.border.size.height-self.image.size.height))) displY = - displY;
    newCoords = CGPointMake(newCoords.x + displX, newCoords.y + displY);
    self.location = newCoords;

    self.age++;
    if ((float)self.age/(float)self.maxAge >= 0.9) self.isNearDeath = YES;
    dispatch_sync(dispatch_get_main_queue(),
            ^{
                [self.anthillView drawAnt:self];
            });
}

- (BOOL)checkIfTooOldToLive
{
    if (self.age >= self.maxAge) return YES;
    return NO;
}

@end
