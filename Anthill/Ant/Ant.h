//
//  Ant.h
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AntHillView;

@interface Ant : NSObject

@property NSUInteger age;
@property CGPoint location;
@property NSUInteger maxAge;
@property UIImage *image;
@property CGRect border;
@property AntHillView *anthillView;
@property BOOL isNearDeath;
@property float timeToMove;

@property (strong, nonatomic) dispatch_source_t moveTimer;
@property (strong, nonatomic) dispatch_source_t actionTimer;

- (void) makeRandomMove;
- (void) performAction;
- (BOOL) checkIfTooOldToLive;
- (void) startLiving;
- (void) stopLiving;

@end
