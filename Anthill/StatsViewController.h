//
//  StatsViewController.h
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

@class Stats;
#import <UIKit/UIKit.h>

@interface StatsViewController : UIViewController

@property Stats *stats;

@end