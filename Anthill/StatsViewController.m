//
//  StatsViewController.m
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#define kAntsViewController 0
#import "StatsViewController.h"
#import "AntsViewController.h"
#import "Stats.h"


@interface StatsViewController ()
@property (strong, nonatomic) IBOutlet UIButton *addMotherButton;
@property (strong, nonatomic) IBOutlet UILabel *mothersCounterLabel;
@property (strong, nonatomic) IBOutlet UILabel *workerCounterLabel;
@property (strong, nonatomic) IBOutlet UILabel *fighterCounterLabel;
@property (strong, nonatomic) IBOutlet UILabel *nurseCounterLabel;

- (IBAction)addMotherButtonPressed:(UIButton *)sender;


@end

@implementation StatsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.stats = [Stats defaultManager];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateStats) name:@"Stats changed" object:nil];
}

- (void)updateStats
{
    self.mothersCounterLabel.text = [NSString stringWithFormat:@"%d",self.stats.mothers];
    self.fighterCounterLabel.text = [NSString stringWithFormat:@"%d",self.stats.fighters];
    self.nurseCounterLabel.text = [NSString stringWithFormat:@"%d",self.stats.nurses];
    self.workerCounterLabel.text = [NSString stringWithFormat:@"%d",self.stats.workers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addMotherButtonPressed:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Add the ant now" object:[NSNumber numberWithUnsignedInteger:Mother]];
}

@end