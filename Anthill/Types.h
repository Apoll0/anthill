//
//  Types.h
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 12.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#ifndef Anthill_Types_h
#define Anthill_Types_h

#define kNumberOfTypes 4

typedef NS_ENUM(NSInteger , AntType)
{
    Mother = 1,
    Worker,
    Fighter,
    Nurse
};

#endif
