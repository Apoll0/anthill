//
//  OptionsViewController.h
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 15.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *maxMoveEdit;
@property (strong, nonatomic) IBOutlet UITextField *motherLiveEdit;
@property (strong, nonatomic) IBOutlet UITextField *fighterLiveEdit;
@property (strong, nonatomic) IBOutlet UITextField *workerLiveEdit;
@property (strong, nonatomic) IBOutlet UITextField *nurseLiveEdit;
@property (strong, nonatomic) IBOutlet UITextField *moveTimeEdit;
@property (strong, nonatomic) IBOutlet UISwitch *produceMothersSw;
- (IBAction)viewWasClicked:(id)sender;

@end
