//
//  AntsViewController.m
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//



#import "AntsViewController.h"
#import "AntHillView.h"
#import "AntFighter.h"
#import "AntNurse.h"
#import "AntWorker.h"
#import "Stats.h"
#import "AntMother.h"

@interface AntsViewController ()

@end

@implementation AntsViewController

- (void) initMainHiveView
{
    CGRect viewFrame = self.view.bounds;
    self.mainView = [[AntHillView alloc] initWithFrame:CGRectMake(viewFrame.origin.x+20, viewFrame.origin.y+30, viewFrame.size.width-40, viewFrame.size.height-150)];
    self.mainView.backgroundColor = [UIColor lightGrayColor];
    self.mainView.delegate = self;
    [self.view addSubview:self.mainView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.stats = [Stats defaultManager];
    self.hive = [[NSMutableSet alloc] init];

    //Register for "Add ant" call
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addAntMessageRecieved:) name:@"Add the ant now" object:nil];
}

- (void)addAntMessageRecieved: (NSNotification *)notification
{
    AntType antType = (AntType)[((NSNumber *) [notification object]) unsignedIntegerValue];
    [self addAnt:antType];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!self.mainView) [self initMainHiveView];
}

- (void)addAnt:(AntType)type
{
    switch (type)
    {
        case Mother:
        {

            AntMother *newAnt = [[AntMother alloc] init];
            newAnt.anthillView = self.mainView;
            [self.hive addObject:newAnt];
            [newAnt startLiving];
            self.stats.mothers++;
        }
            break;
        case Fighter:
        {
            AntFighter *newAnt = [[AntFighter alloc] init];
            newAnt.anthillView = self.mainView;
            [self.hive addObject:newAnt];
            [newAnt startLiving];
            self.stats.fighters++;
        }
            break;
        case Nurse:
        {
            AntNurse *newAnt = [[AntNurse alloc] init];
            newAnt.anthillView = self.mainView;
            [self.hive addObject:newAnt];
            [newAnt startLiving];
            self.stats.nurses++;
        }
            break;
        case Worker:
        {
            AntWorker *newAnt = [[AntWorker alloc] init];
            newAnt.anthillView = self.mainView;
            [self.hive addObject:newAnt];
            [newAnt startLiving];
            self.stats.workers++;
        }
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end