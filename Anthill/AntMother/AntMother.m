//
// Created by SIARHEI TSISHKEVICH on 11.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "Ant.h"
#import "AntMother.h"
#import "AntHillView.h"


@implementation AntMother
{

}
- (id)init
{
    self = [super init];
    if (self)
    {
        self.image = [UIImage imageNamed:@"Mother.png"];
        self.maxAge = [[[NSUserDefaults standardUserDefaults] objectForKey:@"kMaxMotherAge"] unsignedIntegerValue];
    }
    [self.anthillView drawAnt:self];
    return self;
}

- (void)performAction
{
    [super performAction];

    NSUInteger antType;
    //TODO: Enable producing new mothers
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kProduceMothersSw"])
         antType = arc4random_uniform(kNumberOfTypes-1)+1;
    else antType = arc4random_uniform(kNumberOfTypes-1)+2;
    NSNumber *convertedType = [NSNumber numberWithUnsignedInteger:antType];

    //TODO: Adding near same mother
    dispatch_async(dispatch_get_main_queue(),
            ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Add the ant now" object:convertedType];
            });

}



@end