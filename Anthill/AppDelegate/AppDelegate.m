//
//  AppDelegate.m
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "AppDelegate.h"

#define kMaxMovementRange 10
#define kMaxMotherAge 500
#define kMaxFighterAge 150
#define kMaxNurseAge 150
#define kMaxWorkerAge 150
#define kTimeToNewMove 3

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //Checking first start condition and adding defaults
    if (![defaults objectForKey:@"firstRun"])
    {
        [defaults setObject:[NSDate date] forKey:@"firstRun"];
        [defaults setObject:[NSNumber numberWithUnsignedInteger:kMaxMovementRange] forKey:@"kMaxMovementRange"];
        [defaults setObject:[NSNumber numberWithUnsignedInteger:kMaxMotherAge] forKey:@"kMaxMotherAge"];
        [defaults setObject:[NSNumber numberWithUnsignedInteger:kMaxFighterAge] forKey:@"kMaxFighterAge"];
        [defaults setObject:[NSNumber numberWithUnsignedInteger:kMaxWorkerAge] forKey:@"kMaxWorkerAge"];
        [defaults setObject:[NSNumber numberWithUnsignedInteger:kMaxNurseAge] forKey:@"kMaxNurseAge"];
        [defaults setObject:[NSNumber numberWithFloat:kTimeToNewMove] forKey:@"kMoveTime"];
        [defaults setBool:NO forKey:@"kProduceMothersSw"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

}

@end