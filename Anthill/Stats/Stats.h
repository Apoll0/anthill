//
//  Stats.h
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 14.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stats : NSObject

@property (nonatomic) NSUInteger mothers;
@property (nonatomic) NSUInteger workers;
@property (nonatomic) NSUInteger fighters;
@property (nonatomic) NSUInteger nurses;

+ (id) defaultManager;
@end
