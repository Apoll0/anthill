//
//  Stats.m
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 14.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "Stats.h"

@implementation Stats

+ (id)defaultManager
{
    static Stats *defStats = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,
            ^{
        defStats = [[self alloc] init];
    });
    return defStats;
}


- (id)init
{
    self = [super init];
    if (self)
    {
      _mothers = 0;
      _fighters = 0;
      _workers = 0;
      _nurses = 0;
    }

    return self;
}


- (void)setMothers:(NSUInteger)mothers
{
    _mothers = mothers;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Stats changed" object:nil];

}

- (void)setWorkers:(NSUInteger)workers
{
    _workers = workers;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Stats changed" object:nil];
}

- (void)setFighters:(NSUInteger)fighters
{
    _fighters = fighters;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Stats changed" object:nil];
}

- (void)setNurses:(NSUInteger)nurses
{
    _nurses = nurses;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Stats changed" object:nil];
}

@end
