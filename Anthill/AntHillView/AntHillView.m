//
//  AntHillView.m
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "AntHillView.h"
#import "AntMother.h"
#import "AntWorker.h"
#import "AntFighter.h"
#import "AntNurse.h"
#import "Stats.h"


@implementation AntHillView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {

    }
    return self;
}

- (void)drawAnt:(Ant *)ant
{
    if (ant.isNearDeath) ant.image = [UIImage imageNamed:@"Death.png"];
    if ([ant checkIfTooOldToLive])
    {
        [ant stopLiving];
        [self.delegate.hive removeObject:ant];
        if ([ant isKindOfClass:[AntMother class]]) self.delegate.stats.mothers--;
        else if ([ant isKindOfClass:[AntWorker class]]) self.delegate.stats.workers--;
        else if ([ant isKindOfClass:[AntFighter class]]) self.delegate.stats.fighters--;
        else if ([ant isKindOfClass:[AntNurse class]]) self.delegate.stats.nurses--;
    }

    [self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect
{
    UIGraphicsBeginImageContext(self.bounds.size);
    NSMutableSet *tempHive = self.delegate.hive;
    for (Ant *ant in tempHive)
    {
        [ant.image drawAtPoint:ant.location];
    }
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [newImage drawInRect:rect];
}


@end
