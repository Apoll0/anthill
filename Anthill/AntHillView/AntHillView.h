//
//  AntHillView.h
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 11.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ant.h"
//#import "AntMother.h"
#import "AntsViewController.h"


@interface AntHillView : UIView
@property (weak, nonatomic) AntsViewController *delegate;

- (void)drawAnt:(Ant *)ant;

@end
