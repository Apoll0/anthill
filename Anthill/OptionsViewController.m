//
//  OptionsViewController.m
//  Anthill
//
//  Created by SIARHEI TSISHKEVICH on 15.04.14.
//  Copyright (c) 2014 SIARHEI TSISHKEVICH. All rights reserved.
//

#import "OptionsViewController.h"

@interface OptionsViewController ()

@end

@implementation OptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.moveTimeEdit.text = [NSString stringWithFormat:@"%.1f", [[NSUserDefaults standardUserDefaults] floatForKey:@"kMoveTime"]];
    self.maxMoveEdit.text = [NSString stringWithFormat:@"%d", [[NSUserDefaults standardUserDefaults] integerForKey:@"kMaxMovementRange"]];
    self.motherLiveEdit.text = [NSString stringWithFormat:@"%d", [[NSUserDefaults standardUserDefaults] integerForKey:@"kMaxMotherAge"]];
    self.fighterLiveEdit.text = [NSString stringWithFormat:@"%d", [[NSUserDefaults standardUserDefaults] integerForKey:@"kMaxFighterAge"]];
    self.workerLiveEdit.text = [NSString stringWithFormat:@"%d", [[NSUserDefaults standardUserDefaults] integerForKey:@"kMaxWorkerAge"]];
    self.nurseLiveEdit.text = [NSString stringWithFormat:@"%d", [[NSUserDefaults standardUserDefaults] integerForKey:@"kMaxNurseAge"]];
    self.produceMothersSw.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"kProduceMothersSw"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithUnsignedInteger:[self.maxMoveEdit.text integerValue]] forKey:@"kMaxMovementRange"];
    [defaults setObject:[NSNumber numberWithUnsignedInteger:[self.motherLiveEdit.text integerValue]] forKey:@"kMaxMotherAge"];
    [defaults setObject:[NSNumber numberWithUnsignedInteger:[self.fighterLiveEdit.text integerValue]] forKey:@"kMaxFighterAge"];
    [defaults setObject:[NSNumber numberWithUnsignedInteger:[self.workerLiveEdit.text integerValue]] forKey:@"kMaxWorkerAge"];
    [defaults setObject:[NSNumber numberWithUnsignedInteger:[self.nurseLiveEdit.text integerValue]] forKey:@"kMaxNurseAge"];
    [defaults setObject:[NSNumber numberWithFloat:[self.moveTimeEdit.text floatValue]] forKey:@"kMoveTime"];
    [defaults setBool:self.produceMothersSw.on forKey:@"kProduceMothersSw"];
    [[NSUserDefaults standardUserDefaults] synchronize];


}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)viewWasClicked:(id)sender
{
    [self.view endEditing:YES];
}
@end
