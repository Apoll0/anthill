//
// Created by SIARHEI TSISHKEVICH on 11.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "Ant.h"
#import "AntWorker.h"
#import "Types.h"
#import "AntHillView.h"


@implementation AntWorker
{

}
- (id)init
{
    self = [super init];
    if (self)
    {
        self.image = [UIImage imageNamed:@"Worker.png"];
        self.maxAge = [[[NSUserDefaults standardUserDefaults] objectForKey:@"kMaxWorkerAge"] unsignedIntegerValue];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.anthillView drawAnt:self];});
    return self;
}


@end